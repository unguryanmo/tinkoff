//
//  ConversationListViewController.swift
//  Tinkoff
//
//  Created by Maksim Unguryan on 04/10/2018.
//  Copyright © 2018 Maksim Unguryan. All rights reserved.
//

import UIKit

struct ConversationCell: ConversationListCellConfiguration {
    var name: String?;
    var message: String?;
    var date: Date?;
    var online: Bool;
    var hasUnreadMessages: Bool;
}

class ConversationListViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private let identifier = String(describing: ConversationListCell.self)
    
    private let sections = ["Online", "History"]
    var onlineConversation: [ConversationCell] = [
        ConversationCell(name: "Vasya Q.", message: nil, date: nil, online: true, hasUnreadMessages: false),
        ConversationCell(name: "Vasya W.", message: "Hello", date: Date(), online: true, hasUnreadMessages: false),
        ConversationCell(name: "Vasya E.", message: "Hello", date: Date(), online: true, hasUnreadMessages: true),
        ConversationCell(name: "Vasya R.", message: "Ahahahahahhahahahahahhahahahahahahhahahahaha", date: Date(), online: true, hasUnreadMessages: false),
        ConversationCell(name: "Vasya T.", message: "Ahahahahahhahahahahahhahahahahahahhahahahaha", date: Date(), online: true, hasUnreadMessages: true),
        
    ]
    var historyConversation: [ConversationCell] = [
        ConversationCell(name: "Petya Q.", message: "NNNNNN", date: Date(), online: false, hasUnreadMessages: false),
        ConversationCell(name: "Petya W.", message: "Hello", date: Date(), online: false, hasUnreadMessages: false),
        ConversationCell(name: "Petya E.", message: "Hello", date: Date(), online: false, hasUnreadMessages: true),
        ConversationCell(name: "Petya R.", message: "Ahahahahahhahahahahahhahahahahahahhahahahaha", date: Date(), online: false, hasUnreadMessages: false),
        ConversationCell(name: "Petya T.", message: "Ahahahahahhahahahahahhahahahahahahhahahahaha", date: Date(), online: false, hasUnreadMessages: true),
    ]
    private var sectionData: [Int: [ConversationCell]] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let profileButton = UIButton(type: .system)
        profileButton.setImage(#imageLiteral(resourceName: "ProfileIcon").withRenderingMode(.alwaysOriginal), for: .normal)
        profileButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        profileButton.addTarget(self, action: #selector(showProfile), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: profileButton)

        tableView.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.sectionFooterHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        
        // Заполняю массив данными с датой отличной от сегоднящней
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy"
        let date = dateFormatter.date(from: "04.10.2018")
        onlineConversation.append(ConversationCell(name: "Vasya Y.", message: "Hello", date: date, online: true, hasUnreadMessages: false))
        onlineConversation.append(ConversationCell(name: "Vasya U.", message: "Hello", date: date, online: true, hasUnreadMessages: false))
        onlineConversation.append(ConversationCell(name: "Vasya I.", message: "Ahahahahahhahahahahahhahahahahahahhahahahaha", date: date, online: true, hasUnreadMessages: false))
        onlineConversation.append(ConversationCell(name: "Vasya O.", message: "Ahahahahahhahahahahahhahahahahahahhahahahaha", date: date, online: true, hasUnreadMessages: true))
        onlineConversation.append(ConversationCell(name: "Vasya P.", message: "Ok", date: date, online: true, hasUnreadMessages: true))
        
        historyConversation.append(ConversationCell(name: "Petya Y.", message: "Hello", date: date, online: false, hasUnreadMessages: false))
        historyConversation.append(ConversationCell(name: "Petya U.", message: "Hello", date: date, online: false, hasUnreadMessages: false))
        historyConversation.append(ConversationCell(name: "Petya I.", message: "Ahahahahahhahahahahahhahahahahahahhahahahaha", date: date, online: false, hasUnreadMessages: false))
        historyConversation.append(ConversationCell(name: "Petya O.", message: "Ahahahahahhahahahahahhahahahahahahhahahahaha", date: date, online: false, hasUnreadMessages: true))
        historyConversation.append(ConversationCell(name: "Petya P.", message: "Ok", date: date, online: false, hasUnreadMessages: true))
        
        self.sectionData = [
            0: onlineConversation,
            1: historyConversation
        ]
    }
    
    @objc func showProfile() {
        if let profileViewController = UIStoryboard(name: "Profile", bundle: nil).instantiateInitialViewController() {
            self.present(profileViewController, animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension ConversationListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (sectionData[section]?.count)!
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as? ConversationListCell else {
            return UITableViewCell()
        }
        
        cell.setup(coversationCell: sectionData[indexPath.section]![indexPath.row])
        
        return cell
    }
}

extension ConversationListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let viewController = UIStoryboard(name: "Conversation", bundle: nil).instantiateInitialViewController() {
            viewController.navigationItem.title = sectionData[indexPath.section]![indexPath.row].name
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
