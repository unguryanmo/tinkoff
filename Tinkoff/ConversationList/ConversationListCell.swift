//
//  ConversationListCell.swift
//  Tinkoff
//
//  Created by Maksim Unguryan on 04/10/2018.
//  Copyright © 2018 Maksim Unguryan. All rights reserved.
//

import UIKit

class ConversationListCell: UITableViewCell, ConversationListCellConfiguration {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    private var _date: Date?
    
    var name: String? {
        set {
            self.nameLabel.text = newValue
        }
        get {
            return self.nameLabel.text
        }
    }
    var message: String? {
        set {
            if let message = newValue {
                self.messageLabel.text = message
            } else {
                self.messageLabel.text = "No messages yet"
            }
        }
        get {
            return self.messageLabel.text
        }
    }
    var date: Date? {
        set {
            if let date = newValue {
                self._date = date
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy"
                
                if (dateFormatter.string(from: self._date!) == dateFormatter.string(from: Date())) {
                    dateFormatter.dateFormat = "HH:mm"
                } else {
                    dateFormatter.dateFormat = "dd MMM"
                }
                
                self.dateLabel.text = dateFormatter.string(from: self._date!)
            } else {
                self._date = nil
                self.dateLabel.text = ""
            }
        }
        get {
            return self._date
        }
    }
    var online: Bool {
        set {
            if (newValue) {
                self.backgroundColor = #colorLiteral(red: 1, green: 0.9882352941, blue: 0.7960784314, alpha: 1)
            } else {
                self.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
        }
        get {
            return self.backgroundColor == #colorLiteral(red: 1, green: 0.9882352941, blue: 0.7960784314, alpha: 1) ? true : false
        }
    }
    var hasUnreadMessages: Bool {
        set {
            if (message != "No messages yet") {
                if (newValue) {
                    self.messageLabel.font = UIFont.boldSystemFont(ofSize: 17)
                } else {
                    self.messageLabel.font = UIFont.systemFont(ofSize: 17)
                }
            } else {
                self.messageLabel.font = UIFont(name: "Chalkduster", size: 17)
            }
        }
        get {
            return self.messageLabel.font == UIFont.boldSystemFont(ofSize: 17) ? true : false
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setup(coversationCell cell: ConversationListCellConfiguration) {
        self.name = cell.name
        self.message = cell.message
        self.date = cell.date
        self.online = cell.online
        self.hasUnreadMessages = cell.hasUnreadMessages
    }
    
}
