//
//  ConversationViewController.swift
//  Tinkoff
//
//  Created by Maksim Unguryan on 05/10/2018.
//  Copyright © 2018 Maksim Unguryan. All rights reserved.
//

import UIKit

struct Message: MessageCellConfiguration {
    var incoming: Bool
    var message: String?
}

class ConversationViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    
    private let incomingMessageIdentifier = String(describing: InсomingMessageCell.self)
    private let outgoingMessageIdentifier = String(describing: OutgoingMessageCell.self)
    
    let messages: [Message] = [
        Message(incoming: true, message: "Lor"),
        Message(incoming: false, message: "Lor"),
        Message(incoming: true, message: "Lorem ipsum dolor sit amet, co"),
        Message(incoming: false, message: "Lorem ipsum dolor sit amet, co"),
        Message(incoming: true, message: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec."),
        Message(incoming: false, message: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec.")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.allowsSelection = false
        
        tableView.register(UINib(nibName: self.incomingMessageIdentifier, bundle: nil), forCellReuseIdentifier: self.incomingMessageIdentifier)
        tableView.register(UINib(nibName: self.outgoingMessageIdentifier, bundle: nil), forCellReuseIdentifier: self.outgoingMessageIdentifier)
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.sectionFooterHeight = UITableViewAutomaticDimension
        
        tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension ConversationViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if messages[indexPath.row].incoming {
            guard let inсomingMessageCell = tableView.dequeueReusableCell(withIdentifier: self.incomingMessageIdentifier, for: indexPath) as? InсomingMessageCell else {
                return UITableViewCell()
            }
            
            inсomingMessageCell.message = messages[indexPath.row].message
            
            return inсomingMessageCell
        } else {
            guard let outgoingMessageCell = tableView.dequeueReusableCell(withIdentifier: self.outgoingMessageIdentifier, for: indexPath) as? OutgoingMessageCell else {
                return UITableViewCell()
            }
            
            outgoingMessageCell.message = messages[indexPath.row].message
            
            return outgoingMessageCell
        }
    }
}
