//
//  InputMessageCell.swift
//  Tinkoff
//
//  Created by Maksim Unguryan on 06/10/2018.
//  Copyright © 2018 Maksim Unguryan. All rights reserved.
//

import UIKit

class InсomingMessageCell: UITableViewCell, MessageCellConfiguration {
    @IBOutlet weak var messageLabel: UILabel!
    
    private let cornerRadiusForMessage = CGFloat(15);
    
    var message: String? {
        set {
            if let message = newValue {
                self.messageLabel.text = message
            } else {
                self.messageLabel.text = ""
            }
        }
        get {
            return self.messageLabel.text
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        messageLabel.layer.masksToBounds = true
        messageLabel.layer.cornerRadius = self.cornerRadiusForMessage
        
    }
}
