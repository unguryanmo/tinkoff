//
//  MessageCellConfiguration.swift
//  Tinkoff
//
//  Created by Maksim Unguryan on 07/10/2018.
//  Copyright © 2018 Maksim Unguryan. All rights reserved.
//

import Foundation

protocol MessageCellConfiguration {
    var message: String? {get set}
}
