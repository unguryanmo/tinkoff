//
//  ProfileData.swift
//  Tinkoff
//
//  Created by Maksim Unguryan on 21/10/2018.
//  Copyright © 2018 Maksim Unguryan. All rights reserved.
//

import UIKit

struct ProfileData {
    var name: String?
    var about: String?
    var image: UIImage?
}
