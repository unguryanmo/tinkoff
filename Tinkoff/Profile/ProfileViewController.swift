//
//  ProfileViewController.swift
//  Tinkoff
//
//  Created by Maksim Unguryan on 23/09/2018.
//  Copyright © 2018 Maksim Unguryan. All rights reserved.
//

import UIKit
import Photos
import AVFoundation

class ProfileViewController: UIViewController, UINavigationControllerDelegate {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    @IBOutlet weak var saveViaGcdButton: UIButton!
    @IBOutlet weak var saveViaOperations: UIButton!
    
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var aboutLabel: UILabel!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var aboutTextView: UITextView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    var name: String? {
        set {
            self.nameLabel.text = newValue
        }
        get {
            return self.nameLabel.text
        }
    }
    
    var about: String? {
        set {
            self.aboutLabel.text = newValue
        }
        get {
            return self.aboutLabel.text
        }
    }
    
    var image: UIImage? {
        set {
            self.profileImage.image = newValue
        }
        get {
            return self.profileImage.image
        }
    }
    
    var lastImage: UIImage?
    
    let imagePicker = UIImagePickerController()
    
    var initialTouchPoint: CGPoint = CGPoint(x: 0,y: 0)
    
    let profileDataManagerGcd = ProfileDataManagerGCD()
    
    var editMode: Bool {
        set {
            if newValue {
                self.nameLabel.isHidden = true
                self.aboutLabel.isHidden = true
                self.editButton.isHidden = true
                
                self.cameraButton.isHidden = false
                self.nameTextField.isHidden = false
                self.aboutTextView.isHidden = false
                
                self.saveViaGcdButton.isHidden = false
                self.saveViaOperations.isHidden = false
            } else {
                self.nameLabel.isHidden = false
                self.aboutLabel.isHidden = false
                self.editButton.isHidden = false
                
                self.cameraButton.isHidden = true
                self.nameTextField.isHidden = true
                self.aboutTextView.isHidden = true
                
                self.saveViaGcdButton.isHidden = true
                self.saveViaOperations.isHidden = true
            }
        }
        get {
            return !self.nameTextField.isHidden
        }
    }
    
    // Добавил возможность закрыть view профайла свайпом вниз, можно оставить эту возможность? Просто крестик мне нравится куда меньше чем вариант свайпа.
    @IBAction func panGestureRecognizerHandler(_ sender: UIPanGestureRecognizer) {
        let touchPoint = sender.location(in: self.view?.window)
        
        if sender.state == UIGestureRecognizerState.began {
            initialTouchPoint = touchPoint
        } else if sender.state == UIGestureRecognizerState.changed {
            if touchPoint.y - initialTouchPoint.y > 0 {
                self.view.frame = CGRect(x: 0, y: touchPoint.y - initialTouchPoint.y, width: self.view.frame.size.width, height: self.view.frame.size.height)
            }
        } else if sender.state == UIGestureRecognizerState.ended || sender.state == UIGestureRecognizerState.cancelled {
            if touchPoint.y - initialTouchPoint.y > 100 {
                self.dismiss(animated: true, completion: nil)
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
                })
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imagePicker.delegate = self
        self.aboutTextView.delegate = self
        
        self.activityIndicator.hidesWhenStopped = true
        self.readData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let padding = self.cameraButton.frame.size.width / 4
        self.cameraButton.contentEdgeInsets = UIEdgeInsets(
            top: padding,
            left: padding,
            bottom: padding,
            right: padding
        )
        self.closeButton.contentEdgeInsets = UIEdgeInsets(
            top: padding,
            left: padding,
            bottom: padding,
            right: padding
        )
        
        let radius = self.cameraButton.frame.size.width / 2
        self.cameraButton.layer.cornerRadius = radius
        self.closeButton.layer.cornerRadius = radius
        self.profileImage.layer.cornerRadius = radius
        
        let radiusButton = self.editButton.frame.size.height / 4
        self.editButton.layer.cornerRadius = radiusButton
        self.saveViaOperations.layer.cornerRadius = radiusButton
        self.saveViaGcdButton.layer.cornerRadius = radiusButton
        
        self.editButton.layer.borderWidth = 2
        self.editButton.layer.borderColor = UIColor.black.cgColor
        
        self.saveViaOperations.layer.borderWidth = 2
        self.saveViaOperations.layer.borderColor = UIColor.black.cgColor
        
        self.saveViaGcdButton.layer.borderWidth = 2
        self.saveViaGcdButton.layer.borderColor = UIColor.black.cgColor
        
        self.aboutTextView.layer.borderWidth = 1
        self.aboutTextView.layer.borderColor = #colorLiteral(red: 0.9097049236, green: 0.909860909, blue: 0.9096950889, alpha: 1)
        self.aboutTextView.layer.cornerRadius = 5
    }
    
    private func presentPhotoLibrary() {
        self.imagePicker.sourceType = .photoLibrary
        self.imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    private func presentCamera() {
        self.imagePicker.sourceType = .camera
        self.imagePicker.cameraCaptureMode = .photo
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    private func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func showAlertWithURLToSettings(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: {action in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                self.showAlert(title: "Move error", message: "Some error when you move to settings")
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: nil)
            }
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    private func showAlertWithRetryButton(title: String, message: String, handler: ((UIAlertAction) -> Swift.Void)?) {
        let alert = UIAlertController()
        
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        alert.addAction(UIAlertAction(title: "Повторить", style: .default, handler: handler))
        
        self.present(alert, animated: true)
    }
    
    private func handlerPhotoLibrary(action: UIAlertAction) {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        
        switch photoAuthorizationStatus {
        case .authorized:
            self.presentPhotoLibrary()
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ newStatus in
                if newStatus ==  PHAuthorizationStatus.authorized {
                    self.presentPhotoLibrary()
                } else {
                    self.showAlert(title: "Access to Photo", message: "Sorry, you app not have access to photo")
                }
            })
        case .restricted:
            self.showAlert(title: "Access to Photo", message: "User do not have access to photo album.")
        case .denied:
            self.showAlertWithURLToSettings(title: "Access to Photo", message: "User has denied the permission.")
        }
    }
    
    private func handlerCamera(action: UIAlertAction) {
        let cameraAuthorizationStatus = AVCaptureDevice.authorizationStatus(for: .video)
        switch cameraAuthorizationStatus {
        case .authorized:
            self.presentCamera()
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { granted in
                if granted {
                    self.presentCamera()
                } else {
                    self.showAlert(title: "Access to Camera", message: "Sorry, you app not have access to camera")
                }
            })
        case .restricted:
            self.showAlert(title: "Access to Camera", message: "User do not have access to camera.")
        case .denied:
            self.showAlertWithURLToSettings(title: "Access to Camera", message: "User has denied the permission.")
        }
    }
    
    @IBAction func clickOnCamera(_ sender: Any) {
        self.imagePicker.allowsEditing = false
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(
            title: "Установить из галлереи",
            style: .default,
            handler: self.handlerPhotoLibrary
        ))
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            actionSheet.addAction(UIAlertAction(
                title: "Сделать фото",
                style: .default,
                handler: self.handlerCamera
            ))
        }
        actionSheet.addAction(UIAlertAction(
            title: "Отмена",
            style: .cancel,
            handler: nil
        ))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func clickOnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickOnEdit(_ sender: Any) {
        self.editMode = true
        
        self.nameTextField.text = self.name
        self.aboutTextView.text = self.about
        
        if self.aboutTextView.text.isEmpty {
            self.aboutTextView.text = "Tell about you"
            self.aboutTextView.textColor = #colorLiteral(red: 0.7684687972, green: 0.7682297826, blue: 0.7930106521, alpha: 1)
        } else {
            self.aboutTextView.textColor = UIColor.black
        }
    }
    
    @IBAction func clickOnSaveViaGcd(_ sender: Any) {
        self.saveData()
    }
    
    @IBAction func clickOnSaveViaOperations(_ sender: Any) {
        self.showAlert(title: "Прошу прощения", message: "Не успел")
    }
    
    func readData() {
        self.activityIndicator.startAnimating()
        self.profileDataManagerGcd.readInformation(successfulHandler: readSuccessfulHandler, errorHandler: readErrorHandler)
    }
    
    func saveData() {
        var data = ProfileData()
        
        if nameTextField.text != name {
            print("Change name")
            data.name = nameTextField.text == nil ? "" : nameTextField.text
        }
        if aboutTextView.text != about {
            print("Change about")
            data.about = aboutTextView.textColor == #colorLiteral(red: 0.7684687972, green: 0.7682297826, blue: 0.7930106521, alpha: 1) ? "" : aboutTextView.text
        }
        if self.image != self.lastImage {
            print("Change image")
            data.image = self.image
        }
        
        if data.name != nil || data.about != nil || data.image != nil {
            self.saveViaGcdButton.isEnabled = false
            self.saveViaOperations.isEnabled = false
            
            self.activityIndicator.startAnimating()
            self.profileDataManagerGcd.saveInformation(profileData: data, successfulHandler: saveSuccessfulHandler, errorHandler: saveErrorHandler)
        } else {
            self.showAlert(title: "Данные остались без изменений", message: "")
        }
    }
    
    func readSuccessfulHandler(profileData data: ProfileData) {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            
            self.editMode = false
            
            if let name = data.name {
                self.name = name
            } else {
                self.name = ""
            }
            if let about = data.about {
                self.about = about
            } else {
                self.about = ""
            }
            if let image = data.image {
                self.image = image
            }
            self.lastImage = self.image
        }
    }
    
    func readErrorHandler() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            
            self.showAlertWithRetryButton(title: "Ошибка", message: "Не удалось прочитать данные", handler: { _ in
                self.readData()
            })
        }
    }
    
    func saveSuccessfulHandler() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            
            self.showAlert(title: "Данные сохранены", message: "")
            
            self.saveViaGcdButton.isEnabled = true
            self.saveViaOperations.isEnabled = true
            
            self.readData()
        }
    }
    
    func saveErrorHandler() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            
            self.showAlertWithRetryButton(title: "Ошибка", message: "Не удалось сохранить данные", handler: { _ in
                self.saveData()
            })
            
            self.saveViaGcdButton.isEnabled = true
            self.saveViaOperations.isEnabled = true
        }
    }
}

extension ProfileViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        self.profileImage.image = chosenImage
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

extension ProfileViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == #colorLiteral(red: 0.7684687972, green: 0.7682297826, blue: 0.7930106521, alpha: 1) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Tell about you"
            textView.textColor = #colorLiteral(red: 0.7684687972, green: 0.7682297826, blue: 0.7930106521, alpha: 1)
        }
    }
}
