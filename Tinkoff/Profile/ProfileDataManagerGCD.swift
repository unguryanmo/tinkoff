//
//  ProfileDataManagerGCD.swift
//  Tinkoff
//
//  Created by Maksim Unguryan on 21/10/2018.
//  Copyright © 2018 Maksim Unguryan. All rights reserved.
//

import UIKit

class ProfileDataManagerGCD {
    let nameFile = "name"
    let aboutFile = "about"
    let imageFile = "image"
    
    let nameFileUrl: URL
    let aboutFileUrl: URL
    let imageFileUrl: URL
    
    init() {
        let directory = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        
        nameFileUrl = directory.appendingPathComponent("name").appendingPathExtension("txt")
        aboutFileUrl = directory.appendingPathComponent("about").appendingPathExtension("txt")
        imageFileUrl = directory.appendingPathComponent("image").appendingPathExtension("png")
    }
    
    private let concurrentQueue =
        DispatchQueue(
            label: "com.concurent",
            attributes: .concurrent)
    
    func saveInformation(profileData data: ProfileData, successfulHandler: @escaping () -> Void, errorHandler: @escaping () -> Void) {
        self.concurrentQueue.async {
            do {
                if let name = data.name {
                    try name.write(to: self.nameFileUrl, atomically: false, encoding: .utf8)
                }
                if let about = data.about {
                    print(about)
                    try about.write(to: self.aboutFileUrl, atomically: false, encoding: .utf8)
                }
                if let image = data.image {
                    if let imageData = UIImageJPEGRepresentation(image, 1) {
                        try imageData.write(to: self.imageFileUrl)
                    }
                }
                successfulHandler()
            } catch {
                errorHandler()
            }
        }
    }
    
    func readInformation(successfulHandler: @escaping (_ data: ProfileData) -> Void, errorHandler: @escaping () -> Void) {
        do {
            let fileManager = FileManager.default
            
            var data = ProfileData()
            
            if fileManager.fileExists(atPath: nameFileUrl.path) {
                data.name = try String(contentsOf: self.nameFileUrl, encoding: .utf8)
            }
            if fileManager.fileExists(atPath: aboutFileUrl.path) {
                data.about = try String(contentsOf: self.aboutFileUrl, encoding: .utf8)
            }
            if fileManager.fileExists(atPath: imageFileUrl.path) {
                let imageData = try Data(contentsOf: self.imageFileUrl)
                data.image = UIImage(data: imageData)
            }
            successfulHandler(data)
        } catch {
            errorHandler()
        }
    }
}
