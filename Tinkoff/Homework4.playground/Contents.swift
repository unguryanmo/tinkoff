class Ceo {
    let name: String
    weak var manager: ProductManager?
    
    lazy var communicateWithManager = { [weak self] in
        self!.manager?.printCompany()
    }
    lazy var printProductManager = { [weak self] in
        self!.manager?.printProductManager()
    }
    lazy var printDevelopers = { [weak self] in
        self!.manager?.printDevelopers()
    }
    
    init(name: String) {
        self.name = name
    }
    deinit {
        print("CEO (\(self.name)) уничтожен")
    }
    
    func getMessage(from name: String, message text: String) {
        print("Сeo (\(self.name)) получил сообщение от \(name): \"\(text)\"")
    }
}

class ProductManager {
    let name: String
    weak var ceo: Ceo?
    weak var developer1: Developer?
    weak var developer2: Developer?
    weak var developer3: Developer?
    
    init(name: String) {
        self.name = name
    }
    deinit {
        print("Менеджер (\(self.name)) уничтожен")
    }
    
    func printCompany() {
        print("CEO: \(self.ceo!.name)")
        self.printProductManager()
        self.printDevelopers()
    }
    func printProductManager() {
        print("Менеджер: \(self.name)")
    }
    func printDevelopers() {
        print("Разработчик: \(self.developer1!.name)")
        print("Разработчик: \(self.developer2!.name)")
        print("Разработчик: \(self.developer3!.name)")
    }
    func getMessage(from name: String, message text: String) {
        print("Менеджер (\(self.name)) получил сообщение от \(name): \"\(text)\"")
    }
    func resendToCeo(from name: String, message text: String) {
        ceo?.getMessage(from: name, message: text)
    }
    func resendToDeveloper(from name: String, to: String, message text: String) {
        switch to {
        case developer1?.name:
            developer1?.getMessage(from: name, message: text)
        case developer2?.name:
            developer2?.getMessage(from: name, message: text)
        case developer3?.name:
            developer3?.getMessage(from: name, message: text)
        default:
            print("\(to) не найден")
        }
    }
}

class Developer {
    let name: String
    weak var manager: ProductManager?
    
    init(name: String) {
        self.name = name
    }
    deinit {
        print("Разработчик (\(self.name)) уничтожен")
    }
    
    func getMessage(from name: String, message text: String) {
        print("Разработчик \(self.name) получил сообщение от \(name): \"\(text)\"")
    }
    func sendMessageToProductManager(message: String) {
        self.manager?.getMessage(from: self.name, message: message)
    }
    func sendMessageToCeo(message: String) {
        self.manager?.resendToCeo(from: self.name, message: message)
    }
    func sendMessageToDeveloper(to name: String, message: String) {
        self.manager?.resendToDeveloper(from: self.name, to: name, message: message)
    }
}

class Company {
    var ceo: Ceo?
    var manager: ProductManager?
    var developer1: Developer?
    var developer2: Developer?
    var developer3: Developer?
    
    deinit {
        print("Компания уничтожена")
    }
    
    func simulate() {
        developer1?.sendMessageToDeveloper(to: "Jack", message: "Ты говнокодер")
        ceo?.communicateWithManager()
        developer2?.sendMessageToProductManager(message: "Продукт- менеджер, дай мне новую задачу")
        ceo?.printProductManager()
        developer3?.sendMessageToCeo(message: "CEO, я хочу зарплату больше")
        ceo?.printDevelopers()
    }
}

func createCompany() -> Company {
    let company = Company()
    let productManager = ProductManager(name: "John")
    let ceo = Ceo(name: "Tim")
    
    ceo.manager = productManager
    productManager.ceo = ceo
    
    let developer1 = Developer(name: "Alex")
    let developer2 = Developer(name: "Sara")
    let developer3 = Developer(name: "Jack")
    
    developer1.manager = productManager
    developer2.manager = productManager
    developer3.manager = productManager
    
    company.developer1 = developer1
    company.developer2 = developer2
    company.developer3 = developer3
    
    productManager.developer1 = developer1
    productManager.developer2 = developer2
    productManager.developer3 = developer3
    
    company.ceo = ceo
    company.manager = productManager
    
    return company
}

var company: Company? = createCompany()
company?.simulate()
company = nil
